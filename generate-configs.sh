#!/bin/bash

cat > config-a.yml <<HEREDOC
service-a-1:
  script:
    - echo 'job 1 for service A'
service-a-2:
  script:
    - echo 'job 2 for service A'
workflow:
  rules:
    - if: \$CI_COMMIT_BRANCH == "master" && \$CI_PIPELINE_SOURCE != "schedule"
      changes:
        - service-a/**/*
HEREDOC

cat > config-b.yml <<HEREDOC
service-b-1:
  script:
    - echo 'job 1 for service B'
service-b-2:
  script:
    - echo 'job 2 for service B'
workflow:
  rules:
    - if: \$CI_COMMIT_BRANCH == "master" && \$CI_PIPELINE_SOURCE != "schedule"
      changes:
        - service-b/**/*
HEREDOC
